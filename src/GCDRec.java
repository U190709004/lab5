public class GCDRec {

    static int gcd(int a,int b)
    {

        int q = (a / b);
        int r = a - (q * b);

        if(r==0)
            return b;
        else
            return gcd(b, r);

    }

    public static void main(String[] args)
    {

        int sayi1 = Integer.parseInt(args[0]);
        int sayi2 = Integer.parseInt(args[1]);
        System.out.println(gcd(sayi1,sayi2));

    }
}