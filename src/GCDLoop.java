public class GCDLoop {

    public static void main(String[] args)
    {

        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int q = (a/b);
        int r = a-(q*b);


        while(r!=0)
        {
            q = (a / b);
            r = a - (q * b);
            if (r == 0)
                break;


                a = b;
                b = r;

        }

        System.out.println(b);

    }
}